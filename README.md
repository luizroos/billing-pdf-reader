## Arquivo de configuração ##

**config/config.properties**

### Formato ###
{prefixo_arquivo}.{campo}.regex=regex de busca do campo do texto do pdf

### Campos ###
1. clientdocument: documento do cliente
2. duedate: data de vencimento
3. billingcode: código da fatura
4. value: valor da fatura
5. senddate: data de emissão

## Arquivos importados ##

Deve ser do tipo pdf, seguir o formato: prefixo + número qualquer. Sendo que o prefixo deve ser compatível com o arquivo de configuração (vivo, net, etc.

Exemplo:
1. vivo1.pdf
2. net5423.pdf


## Gerar o jar ##
gradle build

## Executando ##

### Exporta o CSV de todos arquivos do diretório {input_dir} para o diretório {output_dir} ###
java -jar build/libs/billing-pdf-reader.jar export input_dir output_dir config_file_path

Exemplo: 
java -jar build/libs/billing-pdf-reader.jar export contas contas config/config.properties

### Exporta o CSV de todos arquivos do diretório {input_dir} imprimindo no console ###
java -jar build/libs/billing-pdf-reader.jar export input_dir console config_file_path

Exemplo:
java -jar build/libs/billing-pdf-reader.jar export contas console config/config.properties

### Imprime todo texto do pdf ###
java -jar build/libs/billing-pdf-reader.jar print input_file config_file_path

Exemplo:
java -jar build/libs/billing-pdf-reader.jar print contas/net1.pdf config/config.properties