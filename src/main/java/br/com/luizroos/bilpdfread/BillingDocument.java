package br.com.luizroos.bilpdfread;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jdk.nashorn.internal.runtime.ParserException;

public class BillingDocument {

	private static Pattern FILE_PATTERN = Pattern.compile("(\\D+)\\d+.pdf");

	private final BillingDocumentParser parser;

	private final String fileType;

	private final Properties properties;

	public BillingDocument(File file, Properties properties) throws IOException {
		if (file == null || properties == null) {
			throw new IllegalArgumentException();
		}
		final Matcher matcher = FILE_PATTERN.matcher(file.getName());
		if (matcher.find()) {
			this.fileType = matcher.group(1);
		} else {
			throw new ParserException("nome do arquivo não pode ser parseado para um tipo valido");
		}
		this.parser = new BillingDocumentParser(file);
		this.properties = properties;
	}

	public String getClientDocument() {
		return parser.findDocumentText(getPropRegex("clientdocument"));
	}

	public String getDueDate() {
		return parser.findDocumentText(getPropRegex("duedate"));
	}

	public String getBillingCode() {
		return parser.findDocumentText(getPropRegex("billingcode"));
	}

	public String getValue() {
		return parser.findDocumentText(getPropRegex("value"));
	}

	public String getSendDate() {
		return parser.findDocumentText(getPropRegex("senddate"));
	}

	private String getPropRegex(String field) {
		return properties.getProperty(fileType + "." + field + ".regex");
	}

	public String getText() {
		return parser.getFileText();
	}

	public String getType() {
		return fileType;
	}

}
