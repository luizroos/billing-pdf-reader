package br.com.luizroos.bilpdfread;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

public class BillingDocumentParser {

	private final String fileText;

	public BillingDocumentParser(String filename) throws IOException {
		this(new File(filename));
	}

	public BillingDocumentParser(File file) throws IOException {
		if (file == null) {
			throw new IllegalArgumentException();
		}
		final PDDocument doc = PDDocument.load(file);
		this.fileText = new PDFTextStripper().getText(doc);
	}

	public String getFileText() {
		return fileText;
	}

	public String findDocumentText(String regex) {
		if (regex == null) {
			return null;
		}
		Pattern pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(fileText);
		while (m.find()) {
			return m.group(1);
		}
		return null;
	}

}
