package br.com.luizroos.bilpdfread;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;

public class CSVExport {

	private final Iterable<BillingDocument> documents;

	private final String separator;

	public CSVExport(Iterable<BillingDocument> documents, String separator) {
		if (documents == null || separator == null) {
			throw new IllegalArgumentException();
		}
		this.documents = documents;
		this.separator = separator;
	}

	public void export(Writer writer) throws IOException {
		if (writer == null) {
			throw new IllegalArgumentException();
		}
		try (BufferedWriter bw = new BufferedWriter(writer)) {
			for (BillingDocument document : documents) {
				write(bw, document.getType());
				write(bw, separator);
				write(bw, document.getClientDocument());
				write(bw, separator);
				write(bw, document.getBillingCode());
				write(bw, separator);
				write(bw, document.getDueDate());
				write(bw, separator);
				write(bw, document.getSendDate());
				write(bw, separator);
				write(bw, document.getValue());
				bw.newLine();
			}
		}
	}

	private void write(BufferedWriter writer, String str) throws IOException {
		if (str != null) {
			writer.write(str);
		}
	}

}
