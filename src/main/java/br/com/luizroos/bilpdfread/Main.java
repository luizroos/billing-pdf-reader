package br.com.luizroos.bilpdfread;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Properties;

public class Main {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		if (args == null || args.length == 0) {
			System.out.println("Parâmetros inválidos, leia https://bitbucket.org/luizroos/billing-pdf-reader");
			System.exit(0);
			return;
		}

		if (args[0].equalsIgnoreCase("export")) {
			export(args[1], args[2], args[3]);
		} else if (args[0].equalsIgnoreCase("print")) {
			print(args[1], args[2]);
		}
	}

	public static void print(String filename, String configFile) throws FileNotFoundException, IOException {
		final Properties config = loadConfig(configFile);
		final BillingDocument document = new BillingDocument(new File(filename), config);
		System.out.println(document.getText());
	}

	public static void export(String inputDir, String outputDir, String configFile) throws FileNotFoundException,
			IOException {
		final Properties config = loadConfig(configFile);

		final ArrayList<BillingDocument> documents = new ArrayList<BillingDocument>();
		for (File file : new File(inputDir).listFiles()) {
			if (file.isFile()) {
				documents.add(new BillingDocument(file, config));
			}
		}

		final CSVExport csvExport = new CSVExport(documents, config.getProperty("separator"));

		Writer writer = null;
		try {
			if (outputDir.equalsIgnoreCase("console")) {
				writer = new PrintWriter(System.out);
			} else {
				writer = new FileWriter(outputDir + "/export.csv");
			}
			csvExport.export(writer);
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	private static Properties loadConfig(String configFile) throws FileNotFoundException, IOException {
		final Properties config = new Properties();
		config.load(new FileInputStream(configFile));
		return config;
	}

}
